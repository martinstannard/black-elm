import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import StartApp.Simple as StartApp


main =
  StartApp.start { model = model, view = view, update = update }

-- MODEL

type alias Card =
  { rank : String
  , suit : String
  }

type alias Hand =
  List Card

type alias Deck =
  List Card


ranks : List String
ranks = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king", "ace"]
--ranks () =
  --List.map toString [1..10]


suitor : (String, String) -> Card
suitor (rank, suit) =
  makeCard(rank, suit)


generateDeck : () -> Deck
generateDeck () = 
  List.map (\n -> suitor(n, "Hearts")) ranks ++
  List.map (\n -> suitor(n, "Clubs")) ranks ++
  List.map (\n -> suitor(n, "Diamonds")) ranks ++
  List.map (\n -> suitor(n, "Spades")) ranks


makeCard : (String, String) -> Card
makeCard (r, s) =
  { rank = r, suit = s}


cardName : Card -> String
cardName card =
  "cards/" ++ card.rank ++ "_of_" ++ card.suit ++ ".png"


type alias Model =
  { deck : Deck
  , hand : Hand
  }

model = initialModel

initialModel : Model
initialModel = 
  { deck = generateDeck() 
  , hand = []
  }

-- UPDATE

type Action = Hit | Stand | New


update : Action -> Model -> Model
update action model =
  case action of
    Hit ->
      { model |
          hand = (List.take 1 model.deck) ++ model.hand,
          deck = List.drop 1 model.deck
      }

    Stand ->
      model

    New ->
      { model |
          hand = [],
          deck = generateDeck()
      }

-- VIEW
cardview : Card -> Html
cardview card =
  span []
    [
      img [ src(cardName(card)), width 80] [ ]
    ]


view : Signal.Address Action -> Model -> Html
view address model =
  div []
    [ div [] (List.map cardview model.deck)
    , div [] (List.map cardview model.hand)
    , button [ onClick address Hit ] [ text "Hit" ]
    , button [ onClick address Stand ] [ text "Stand" ]
    , button [ onClick address New ] [ text "New" ]
    ]

